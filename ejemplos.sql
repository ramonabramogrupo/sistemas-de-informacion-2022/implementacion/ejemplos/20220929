﻿USE ciclistas;

/**
  Consulta 1
  listar el dorsal y nombre de los ciclistas que corren
  para Banesto. 
  Ordenar el resultado por el nombre del ciclista
 **/

  SELECT 
      c.dorsal,c.nombre
    FROM 
      ciclista c
    WHERE 
      c.nomequipo='Banesto'
    ORDER BY
      c.nombre;
    


  /**
  Consulta 2
  Listar el dorsal, nombre y edad de los ciclistas de Banesto
  ordenando los resultados por edad y nombre. 
  El orden debe ser descendente
  **/
  
  
  SELECT 
      c.dorsal,c.nombre,c.edad 
    FROM 
      ciclista c
    WHERE 
      c.nomequipo='Banesto'
    ORDER BY
      edad DESC,nombre DESC;

  SELECT 
      c.dorsal,c.nombre,c.edad 
    FROM 
      ciclista c
    WHERE 
      c.nomequipo='Banesto'
    ORDER BY
      3 DESC,2 DESC;


  /**
    CONSULTA NUMERO 3
    Listar dorsal y nombre de los ciclistas. 
    Mostrar un campo nuevo denominado mayusculas que muestre el
    nombre en mayusculas. (UPPER)
    Mostrar otro campo nuevo denominado minusculas que 
    muestre el nombre en minusculas. (LOWER)
    Mostrar un campo nuevo denominado IMC que se calcule
    como (edad elevado al cuadrado/edad*5) (POW)
   
    */

    SELECT 
          c.dorsal,
          c.nombre,
          UPPER(c.nombre) AS mayusculas,
          LOWER(c.nombre) AS minusculas,
          POW(c.edad,2)/(edad*5) AS imc
          
        FROM 
          ciclista c;
    /**
      CONSULTA NUMERO 4
      Indicar el numero total de ciclistas
      **/

    -- OPCION 1
    SELECT 
        COUNT(*) AS numero 
      FROM 
        ciclista c;

    -- OPCION 2
    SELECT 
        COUNT(*),
        COUNT(c.nombre),
        COUNT(c.edad),
        COUNT(c.nomequipo) 
      FROM 
        ciclista c;

/**
  CONSULTA NUMERO 5
  INDICARME EL NUMERO DE EQUIPOS DONDE CORREN
  CICLISTAS
  **/

  -- OPCION CON DISTINCT EN EL COUNT
    SELECT 
        COUNT(DISTINCT c.nomequipo) 
      FROM 
        ciclista c;
  -- OPCION CON SUBCONSULTAS
    SELECT COUNT(*) 
      FROM 
        (SELECT 
            DISTINCT c.nomequipo 
          FROM 
            ciclista c) AS C1;
    

/**
   Consulta numero 6
   Indicar el numero total de ciclistas
   cuya edad es mayor de 28 años
 **/

  SELECT 
      COUNT(*) AS numero
    FROM 
      ciclista c
    WHERE 
      c.edad>28;

  /**
   Consulta numero 7
   Edad media de los ciclistas
   **/

  SELECT 
      AVG(c.edad) AS media 
    FROM 
      ciclista c;


  -- opcion 2
  -- me quita primero las edades repetidas
  SELECT 
      AVG(DISTINCT c.edad) AS media 
    FROM 
      ciclista c;